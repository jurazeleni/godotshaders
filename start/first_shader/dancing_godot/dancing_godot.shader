shader_type canvas_item;

uniform vec2 amplitude = vec2(10.0, 40.0);
uniform float time_factor = 2;

void vertex() {
	VERTEX.x += cos(TIME * time_factor +VERTEX.x+VERTEX.y) * amplitude.x;
	VERTEX.y += sin(TIME * time_factor +VERTEX.y+VERTEX.x) * amplitude.y;
}